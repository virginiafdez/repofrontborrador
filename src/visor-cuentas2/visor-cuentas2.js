import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
 class VisorCuentas2 extends PolymerElement {
   static get template() {
     return html`
       <style>
         :host {
           display: block;
           border: solid blue;
         }
         .redbg {
           background-color: red;
         }
         .greenbg {
           background-color: green;
         }
         .bluebg {
           background-color: blue;
         }
         .greybg {
           background-color: grey;
         }
       </style>
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <h1> Sus cuentas</h1>
       <dom-repeat items="[[accounts]]">
        <template>
       <h3>IBAN - {{item.IBAN}} </h3>
       <h3>Saldo - {{item.balance}} </h3>
       </template>
       </dom-repeat>

       <iron-ajax
         auto
         id="getAccountsByIdV2"
         url="http://localhost:3000/apitechu/v2/accounts/{{id}}"
         handle-as="json"
         on-response="showData"
       >
       </iron-ajax>
     `;
   }
   static get properties() {
     return {
       IBAN: {
         type: String
       }, balance: {
         type: Number
       }, id: {
         type: Number
       }, accounts: {
         type: Array
       }
     };
   } // end properties
     showData(data) {
       console.log("showData");
       console.log(data.detail.response);
      this.accounts = data.detail.response;

     }
 }  // end class

 window.customElements.define('visor-cuentas2', VisorCuentas2);
